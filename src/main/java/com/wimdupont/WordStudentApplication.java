package com.wimdupont;

import com.wimdupont.client.DictionaryApi;
import com.wimdupont.client.WordRepository;
import com.wimdupont.service.WordFetcher;
import com.wimdupont.service.WordTester;

import java.util.List;

public class WordStudentApplication {

    public static void main(String[] args) {
        List<String> words = new WordFetcher().fetch();
        System.out.printf("Words in csv file: %s%n", words.size());

        var wordTester = new WordTester(new WordRepository(), new DictionaryApi());
        wordTester.saveNew(words);
        wordTester.startTest(words);
    }

}
