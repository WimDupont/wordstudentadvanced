package com.wimdupont.model.db;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.wimdupont.model.dto.WordDto;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public record WordCouchDocument(
        List<Row> rows

) {
    @JsonIgnoreProperties(ignoreUnknown = true)
    public record Row(
            WordDto doc) {

    }
}
