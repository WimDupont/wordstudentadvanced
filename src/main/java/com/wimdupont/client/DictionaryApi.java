package com.wimdupont.client;

import com.wimdupont.config.ApplicationProperties;
import com.wimdupont.model.dto.DictionaryDto;
import com.wimdupont.model.dto.WordDto;

import java.util.Arrays;
import java.util.Optional;

public class DictionaryApi {

    private final ApplicationProperties applicationProperties = ApplicationProperties.getInstance();

    public Optional<WordDto> getDictionary(String word) {
        return Client.get(applicationProperties.getDictionaryClientUrl() + word, DictionaryDto[].class)
                .map(Arrays::asList)
                .map(f -> new WordDto(word, f));
    }
}
