package com.wimdupont.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.wimdupont.client.Client;
import com.wimdupont.client.DictionaryApi;
import com.wimdupont.client.WordRepository;
import com.wimdupont.model.dto.WordDto;

import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.text.DefaultCaret;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class WordTester {

    private final WordRepository wordRepository;
    private final DictionaryApi dictionaryApi;
    private final InputMap inputMap;
    private static final String PREVIOUS = "Previous";
    private static final String SHOW = "Show";
    private static final String NEXT = "Next";

    public WordTester(WordRepository wordRepository,
                      DictionaryApi dictionaryApi) {
        this.wordRepository = wordRepository;
        this.dictionaryApi = dictionaryApi;
        this.inputMap = initiateInputMap();
    }

    public void startTest(List<String> words) {
        Collections.shuffle(words);
        showUI(words);
    }

    public void saveNew(List<String> words) {
        List<String> savedWordList = wordRepository.findAllWords();
        System.out.printf("Words in database: %s%n", savedWordList.size());
        AtomicInteger count = new AtomicInteger(0);
        words.stream()
                .filter(csvWord -> !savedWordList.contains(csvWord))
                .forEach(newWord -> {
                    var response = dictionaryApi.getDictionary(newWord);
                    if (response.isEmpty()) {
                        System.out.printf("No results found for '%s', no data saved%n", newWord);
                        count.get();
                    } else {
                        wordRepository.save(response.get());
                        System.out.printf("Saved new word %s%n", response.get());
                        count.getAndIncrement();
                    }
                });

        if (count.get() > 0)
            System.out.printf("Saved %s words%n", count.get());
    }

    private InputMap initiateInputMap() {
        var inputMap = new InputMap();
        inputMap.put(KeyStroke.getKeyStroke("LEFT"), PREVIOUS);
        inputMap.put(KeyStroke.getKeyStroke("H"), PREVIOUS);
        inputMap.put(KeyStroke.getKeyStroke("L"), NEXT);
        inputMap.put(KeyStroke.getKeyStroke("RIGHT"), NEXT);
        inputMap.put(KeyStroke.getKeyStroke("DOWN"), SHOW);
        inputMap.put(KeyStroke.getKeyStroke("SPACE"), SHOW);
        inputMap.put(KeyStroke.getKeyStroke("J"), SHOW);
        return inputMap;
    }

    private void showUI(List<String> words) {
        AtomicInteger index = new AtomicInteger(0);
        JFrame jFrame = new JFrame();
        jFrame.setLayout(new BorderLayout());
        var wordPanel = new JPanel();
        wordPanel.setFocusable(false);
        wordPanel.setLayout(new BoxLayout(wordPanel, BoxLayout.Y_AXIS));
        var wordField = new JLabel(words.get(index.get()));
        wordPanel.add(Box.createRigidArea(new Dimension(20, 20)));
        wordPanel.add(wordField);
        wordField.setFont(new Font(Font.SERIF, Font.BOLD, 30));
        wordPanel.add(Box.createRigidArea(new Dimension(20, 20)));
        var showPanel = createShowPanel(index, wordField, words);
        wordPanel.add(new JScrollPane(showPanel));
        jFrame.add(wordPanel, BorderLayout.CENTER);
        jFrame.add(createButtonPanel(showPanel), BorderLayout.SOUTH);
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jFrame.pack();
        jFrame.setVisible(true);
    }

    private JTextArea createShowPanel(AtomicInteger index, JLabel wordField, List<String> words) {
        var dictionaryPanel = new JTextArea();
        dictionaryPanel.setLineWrap(true);
        dictionaryPanel.setEditable(false);
        dictionaryPanel.setInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT, inputMap);
        dictionaryPanel.setBackground(new Color(0, 0, 51));
        dictionaryPanel.setForeground(new Color(150, 250, 250));
        dictionaryPanel.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 15));
        ((DefaultCaret) dictionaryPanel.getCaret()).setUpdatePolicy(DefaultCaret.NEVER_UPDATE);
        var actionMap = new ActionMap();
        actionMap.put(PREVIOUS, previous(index, wordField, words, dictionaryPanel));
        actionMap.put(SHOW, show(index, words, dictionaryPanel));
        actionMap.put(NEXT, next(index, wordField, words, dictionaryPanel));
        dictionaryPanel.setActionMap(actionMap);
        return dictionaryPanel;
    }

    private JPanel createButtonPanel(JTextArea showPanel) {
        JButton previousButton = new JButton(PREVIOUS);
        previousButton.addActionListener(showPanel.getActionMap().get(PREVIOUS));
        previousButton.setFocusable(false);
        JButton showButton = new JButton(SHOW);
        showButton.setFocusable(false);
        showButton.addActionListener(showPanel.getActionMap().get(SHOW));
        JButton nextButton = new JButton(NEXT);
        nextButton.setFocusable(false);
        nextButton.addActionListener(showPanel.getActionMap().get(NEXT));

        var btnPanel = new JPanel();
        btnPanel.add(previousButton);
        btnPanel.add(showButton);
        btnPanel.add(nextButton);

        return btnPanel;
    }

    private AbstractAction previous(AtomicInteger index, JLabel wordField, List<String> words, JTextArea showPanel) {
        return toActionListener(() -> {
            if (index.get() > 0) {
                wordField.setText(words.get(index.decrementAndGet()));
                showPanel.setText(null);
            }
        });
    }

    private AbstractAction show(AtomicInteger index, List<String> words, JTextArea showPanel) {
        return toActionListener(() -> {
            var word = wordRepository.findByWord(words.get(index.get()));
            word.ifPresent(wordDto -> showPanel.setText(toMeaning(wordDto)));
            showPanel.setVisible(true);
        });
    }

    private AbstractAction next(AtomicInteger index, JLabel wordField, List<String> words, JTextArea showPanel) {
        return toActionListener(() -> {
            if (index.get() < words.size()) {
                wordField.setText(words.get(index.incrementAndGet()));
                showPanel.setText(null);
            }
        });
    }

    private AbstractAction toActionListener(Runnable runnable) {
        return new AbstractAction() {

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                runnable.run();
            }
        };

    }

    private String toMeaning(WordDto wordDto) {
        try {
            return Client.getObjectMapper()
                    .writerWithDefaultPrettyPrinter()
                    .writeValueAsString(wordDto.dictionaryResults());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

}
